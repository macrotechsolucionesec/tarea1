package com.mitocode.tarea1.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.tarea1.dto.VentaDTO;
import com.mitocode.tarea1.exception.ModeloNotFoundException;
import com.mitocode.tarea1.model.Venta;
import com.mitocode.tarea1.service.IVentaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/ventas")
@Api(value = "Servicio REST para las ventas")
public class VentaController {
	@Autowired
	private IVentaService service;
	
	@ApiOperation("Retorna una lista de ventas")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar(){
		return new ResponseEntity<List<Venta>>(service.listar(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public Resource<Venta> listarId(@PathVariable("id") Integer id) {
		Venta v = service.listarId(id);
		if (v == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		
		Resource<Venta> resource = new Resource<Venta>(v);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("venta-resource"));
		
		return resource;
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody VentaDTO venta){
		Venta v = new Venta();
		v = service.registrar(venta);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(v.getIdVenta())
				.toUri();
		
		return ResponseEntity.created(location).build();		
	}
	
	
}
