package com.mitocode.tarea1.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.tarea1.dao.IDetalleVentaDAO;
import com.mitocode.tarea1.dao.IVentaDAO;
import com.mitocode.tarea1.dto.VentaDTO;
import com.mitocode.tarea1.model.DetalleVenta;
import com.mitocode.tarea1.model.Venta;
import com.mitocode.tarea1.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private IVentaDAO dao;
	
	
	@Override
	public Venta registrar(Venta t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public Venta modificar(Venta t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		dao.delete(id);
	}

	@Override
	public Venta listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}
	
	@Override
	public Venta registrar(VentaDTO venta) {
		// Venta del producto
		Venta v = new Venta();
		v.setFecha(venta.getFecha());
		v.setImporte(venta.getImporte());
		v.setPersona(venta.getPersona());
		
		// detalle de la venta
		List<DetalleVenta> L = new ArrayList<DetalleVenta>();
		venta.getDetalleVenta().forEach(d->{
			DetalleVenta dv = new DetalleVenta();
			dv.setCantidad(d.getCantidad());
			dv.setProducto(d.getProducto());
			dv.setVenta(v);
			L.add(dv);
		});
		v.setDetalleVenta(L);
		
		dao.save(v);
		
		return v;
	}
}
