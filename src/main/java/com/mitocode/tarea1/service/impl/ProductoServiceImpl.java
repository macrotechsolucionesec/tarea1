package com.mitocode.tarea1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.tarea1.dao.IProductoDAO;
import com.mitocode.tarea1.model.Producto;
import com.mitocode.tarea1.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService{
	
	@Autowired
	private IProductoDAO dao;
	
	@Override
	public Producto registrar(Producto t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public Producto modificar(Producto t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		dao.delete(id);
	}

	@Override
	public Producto listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Producto> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
