package com.mitocode.tarea1.service;

import com.mitocode.tarea1.dto.VentaDTO;
import com.mitocode.tarea1.model.Venta;

public interface IVentaService extends ICRUD<Venta>{
	Venta registrar(VentaDTO venta);
}
