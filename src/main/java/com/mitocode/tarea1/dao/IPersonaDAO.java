package com.mitocode.tarea1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mitocode.tarea1.model.Persona;


public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
