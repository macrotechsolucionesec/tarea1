package com.mitocode.tarea1.dao;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.mitocode.tarea1.model.Venta;


public interface IVentaDAO extends JpaRepository<Venta, Integer>{

	
}
