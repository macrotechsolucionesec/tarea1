package com.mitocode.tarea1.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import com.mitocode.tarea1.model.Persona;


public class VentaDTO extends ResourceSupport{
	private int idVenta;
	private Persona persona;
	private double importe;
	private LocalDateTime fecha;
	private List<DetalleVentaDTO> detalleVenta;
	
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	public List<DetalleVentaDTO> getDetalleVenta() {
		return detalleVenta;
	}
	public void setDetalleVenta(List<DetalleVentaDTO> detalleVenta) {
		this.detalleVenta = detalleVenta;
	}
	
}
