package com.mitocode.tarea1.dto;

import org.springframework.hateoas.ResourceSupport;
import com.mitocode.tarea1.model.Producto;
import com.mitocode.tarea1.dto.VentaDTO;

public class DetalleVentaDTO extends ResourceSupport{
	private int idDetalleVenta;
	private VentaDTO venta;
	private Producto producto;
	private int cantidad;
	private double precioUnitario;
	private double precioTotal;
	
	
	public int getIdDetalleVenta() {
		return idDetalleVenta;
	}
	public void setIdDetalleVenta(int idDetalleVenta) {
		this.idDetalleVenta = idDetalleVenta;
	}
	public VentaDTO getVenta() {
		return venta;
	}
	public void setVenta(VentaDTO venta) {
		this.venta = venta;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public double getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}
	
	
}
