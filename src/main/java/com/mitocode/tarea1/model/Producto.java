package com.mitocode.tarea1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto", nullable = false)
	private int idProducto;
	@Column(name = "nombre_producto", nullable = false, length = 120)
	private String nombre;
	@Column(name = "marca_producto", nullable = false, length = 120)
	private String marca;
	@Column(name = "estado_producto", nullable = false, length = 10)
	private String estado;
	
	public Producto() {}
	
	public Producto(int idProducto, String nombre, String marca, String estado) {
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.marca = marca;
		this.estado = estado;
	}
	
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
