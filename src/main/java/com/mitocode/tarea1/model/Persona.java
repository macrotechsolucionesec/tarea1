package com.mitocode.tarea1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persona")
public class Persona implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_persona", nullable = false)
	private int idPersona;
	@Column(name = "nombres_persona", nullable = false, length = 120)
	private String nombres;
	@Column(name = "apellidos_persona", nullable = false, length = 120)
	private String apellidos;
	@Column(name = "estado_persona", nullable = false, length = 10)
	private String estado;
	
	public Persona() {}
	
	public Persona(int idPersona, String nombres, String apellidos, String estado) {
		this.idPersona = idPersona;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.estado = estado;
	}
	
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
