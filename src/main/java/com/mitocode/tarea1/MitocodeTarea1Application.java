package com.mitocode.tarea1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MitocodeTarea1Application {

	public static void main(String[] args) {
		SpringApplication.run(MitocodeTarea1Application.class, args);
	}
}
